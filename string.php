<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>document</title>
    </head>
    <body>
       <h2>Contoh Soal</h2>
       <?php
            echo "<h2> Contoh 1 </h2>";
            $Kalimat1 = "Hello World";
            echo "Kalimat pertama : ". $Kalimat1 . "<br>";
            echo "Panjang String : ". strlen($Kalimat1) . "<br>"; 
            echo "Jumlah Kata : ". str_word_count($Kalimat1) . "<br><br>";

            echo "<h3> Contoh 2 </h3>";
            $string2 = "nama saya rezky";
            echo "Kalimat kedua : " . $string2. "<br>";
            echo "Kata pertama : " . substr($string2,0,4) . "<br>";
            echo "Kata kedua : " . substr($string2,5,4) . "<br>" ;
            echo "Kata ketiga : " . substr($string2,10,5) ."<br>";
 
            echo "<h3> COntoh 3 </h3>" ;
            $string3 = "Selamat Pagi" ;
            echo "Kalimat Ketiga : ". $string3. "<br>";
            echo "Ganti kalimat ketiga : ". str_replace("pagi", "malam", $string3);
           
        ?>

    </body>
</html>